import * as babelCore from "babel-core";
import del from "del";
import fs from "fs";
import gulp from "gulp";
import gulp_babel from "gulp-babel";
import gulp_sass from "gulp-sass";
import gulp_sourcemaps from "gulp-sourcemaps";
import gulp_util from "gulp-util";

const SOURCE_PATH = "source";
const OUTPUT_PATH = "dist";

const PATHS = {
    js: { src: `${SOURCE_PATH}/**/*.js`, dest: OUTPUT_PATH },
    html: { src: `${SOURCE_PATH}/**/*.html`, dest: OUTPUT_PATH },
    img: { src: [ `${SOURCE_PATH}/**/*.png`, `${SOURCE_PATH}/**/*.jpg` ], dest: OUTPUT_PATH },
    scss: { src: `${SOURCE_PATH}/**/*.scss`, dest: OUTPUT_PATH },
    deps: {
        src: [
            "node_modules/babel-es6-polyfill/browser-polyfill.min.js",
            "node_modules/requirejs/require.js"
        ],
        dest: `${OUTPUT_PATH}/lib`
    }
};

const BABEL_CONFIG = {
    "plugins": [
        "external-helpers",
        "transform-es2015-modules-amd"
    ],
    "presets": [ "es2015" ],
    "babelrc": false
};

gulp.task("clean", function() {
    del.sync(OUTPUT_PATH);
});

gulp.task("js", () => gulp.src(PATHS.js.src)
    .pipe(gulp_sourcemaps.init())
    .pipe(gulp_babel(BABEL_CONFIG)).on("error", function(error) {
        gulp_util.log(
            gulp_util.colors.red.bold(`${error.name}: `),
            gulp_util.colors.reset.magenta(`${error.message}`),
            gulp_util.colors.reset(`\n${error.codeFrame}`)
        );
        this.emit("end");
    })
    .pipe(gulp_sourcemaps.write(SOURCE_PATH))
    .pipe(gulp.dest(PATHS.js.dest))
);

gulp.task("html", () => gulp.src(PATHS.html.src)
    .pipe(gulp.dest(PATHS.html.dest))
);

gulp.task("img", () => gulp.src(PATHS.img.src)
    .pipe(gulp.dest(PATHS.img.dest))
);

gulp.task("scss", () => gulp.src(PATHS.scss.src)
    .pipe(gulp_sourcemaps.init())
    .pipe(gulp_sass.sync()).on("error", gulp_sass.logError)
    .pipe(gulp_sourcemaps.write(SOURCE_PATH))
    .pipe(gulp.dest(PATHS.scss.dest))
);

gulp.task("deps", () => gulp.src(PATHS.deps.src)
    .pipe(gulp.dest(PATHS.deps.dest))
);

gulp.task("babel-helpers", function() {
    const parts = PATHS.deps.dest.split("/");
    for (let i = 1; i <= parts.length; i++) {
        const dir = parts.slice(0, i).join("/");
        fs.existsSync(dir) || fs.mkdirSync(dir);
    }
    fs.writeFileSync(`${PATHS.deps.dest}/babel-helpers.js`, babelCore.buildExternalHelpers());
});

gulp.task("watch", function() {
    const watchers = [
        gulp.watch(PATHS.html.src, [ "html" ]),
        gulp.watch(PATHS.js.src, [ "js" ]),
        gulp.watch(PATHS.scss.src, [ "scss" ])
    ];
    for (const watcher of watchers) {
        watcher.on("change", outputChange);
    }
    function outputChange(event) {
        console.log(`File ${event.path} was ${event.type}`);
    }
});

gulp.task("default", [ "clean", "js", "html", "img", "scss", "deps", "babel-helpers" ]);
