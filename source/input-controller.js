
const KEY_MAP = {
    38: 0, // Up
    39: 1, // Right
    40: 2, // Down
    37: 3, // Left
    75: 0, // Vim up
    76: 1, // Vim right
    74: 2, // Vim down
    72: 3, // Vim left
    87: 0, // W
    68: 1, // D
    83: 2, // S
    65: 3  // A
};

class InputController {
    constructor(gameBoardElement, restartButton) {
        this.events = {
            move: [ ],
            restart: [ ],
            keepPlaying: [ ]
        };

        this.gameBoardElement = gameBoardElement;
        this.bindButtonPress(restartButton, this.restart.bind(this));

        this.listen();
    }
    bindButtonPress(button, handler) {
        button.addEventListener("click", handler);
        button.addEventListener("touchend", handler);
    }
    on(event, callback) {
        this.events[event].push(callback);
    }
    emit(event, data) {
        for (const i in this.events[event]) {
            this.events[event][i](data);
        }
    }
    restart(e) {
        e.preventDefault();
        this.emit("restart");
    }
    listen() {
        document.addEventListener("keydown", (e) => {
            const modifiers = e.altKey || e.ctrlKey || e.metaKey || e.shiftKey;
            const mapped = KEY_MAP[e.which];

            if (!modifiers && mapped >= 0) {
                e.preventDefault();
                this.emit("move", mapped);
            }
            if (!modifiers && e.which === 85) {
                this.restart();
            }
        });

        let touchStartClientX, touchStartClientY;

        this.gameBoardElement.addEventListener("touchstart", (e) => {
            if (e.touches.length > 1) {
                return;
            }
            touchStartClientX = e.touches[0].clientX;
            touchStartClientY = e.touches[0].clientY;
            e.preventDefault();
        });

        this.gameBoardElement.addEventListener("touchmove", (e) => {
            e.preventDefault();
        });

        this.gameBoardElement.addEventListener("touchend", (e) => {
            if (e.touches.length > 0) {
                return;
            }

            const touchEndClientX = event.changedTouches[0].clientX;
            const touchEndClientY = event.changedTouches[0].clientY;

            const dx = touchEndClientX - touchStartClientX;
            const absDx = Math.abs(dx);

            const dy = touchEndClientY - touchStartClientY;
            const absDy = Math.abs(dy);

            if (Math.max(absDx, absDy) > 10) {
                this.emit("move", absDx > absDy ? (dx > 0 ? 1 : 3) : (dy > 0 ? 2 : 0));
            }
        });
    }
}

export default InputController;
