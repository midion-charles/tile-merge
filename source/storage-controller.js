class StorageController {
    constructor(size) {
        this._size = size;
        this._KEYS = {
            bestScore: `bestScore${size}`,
            gameState: `gameState${size}`
        };
        this.storage = window.localStorage;
        this._bestScore = this.storage.getItem(this._KEYS.bestScore) || 0;
        this._gameState = JSON.parse(this.storage.getItem(this._KEYS.gameState) || null);
    }
    get bestScore() {
        return this._bestScore;
    }
    set bestScore(score) {
        this._bestScore = score;
        this.storage.setItem(this._KEYS.bestScore, score);
    }
    get gameState() {
        return this._gameState;
    }
    set gameState(state) {
        this._gameState = state;
        if (state) {
            this.storage.setItem(this._KEYS.gameState, JSON.stringify(state));
        } else {
            this.storage.removeItem(this._KEYS.gameState);
        }
    }
}

export default StorageController;
